extern crate bytes;
extern crate futures;

use self::futures::sync::mpsc;
use self::bytes::{Bytes, BytesMut};
use self::bytes::BufMut;

use tokio::net::TcpStream;
use tokio::prelude::*;
use tokio::io;

use std::collections::HashMap;
use std::net::SocketAddr;

pub type Tx = mpsc::UnboundedSender<Bytes>;
pub type Rx = mpsc::UnboundedReceiver<Bytes>;

pub struct Shared {
    pub peers: HashMap<SocketAddr, Tx>
}

pub struct Lines {
    pub socket: TcpStream,
    pub rd: BytesMut,
    pub wr: BytesMut
}

impl Shared {
    pub fn new() -> Self {
        return Shared {
            peers: HashMap::new()
        }
    }
}

impl Lines {
    pub fn new(socket: TcpStream) -> Self {
        return Lines {
            socket,
            rd: BytesMut::new(),
            wr: BytesMut::new()
        }
    }

    // Write line to internal buffer for later sending to the socket
    pub fn buffer(&mut self, line: &[u8]) {
        // Ensure there's capacity in the buffer, then push the line to the end of it.
        self.wr.reserve(line.len());
        self.wr.put(line);
    }

    // Write to the socket
    pub fn poll_flush(&mut self) -> Poll<(), io::Error> {
        while self.wr.is_empty() == false {
            // Write the data to the socket
            let n = try_ready!(self.socket.poll_write(&self.wr));

            // Ensure data was written, then discard it
            assert!(n > 0);
            let _ = self.wr.split_to(n);
        }

        return Ok(Async::Ready(()))
    }

    pub fn fill_read_buffer(&mut self) -> Poll<(), io::Error> {
        loop {
            // Ensure the read buffer has capacity, then read into it
            self.rd.reserve(1024);
            let n = try_ready!(self.socket.read_buf(&mut self.rd));

            if n == 0 {
                return Ok(Async::Ready(()));
            }
        }
    }
}

impl Stream for Lines {
    type Item = BytesMut;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        // Read any new data from socket
        let socket_closed = self.fill_read_buffer()?.is_ready();

        // Find lines
        let pos = self.rd.windows(2)
            .enumerate()
            .find(|&(_, bytes)| bytes == b"\r\n")
            .map(|(i, _)| i);

        if let Some(pos) = pos {
            let mut line = self.rd.split_to(pos + 2);
            line.split_off(pos);

            return Ok(Async::Ready(Some(line)));
        }

        if socket_closed {
            return Ok(Async::Ready(None));
        } else {
            return Ok(Async::NotReady);
        }
    }
}