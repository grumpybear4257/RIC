extern crate chrono;

use chrono::prelude::*;
use bytes::BytesMut;
use network::Lines;
use std::sync::Arc;
use std::sync::Mutex;
use network::Shared;
use std::net::SocketAddr;
use network::Rx;
use futures::sync::mpsc;
use tokio::prelude::*;
use tokio::io;
use r2d2_postgres::PostgresConnectionManager;
use r2d2::Pool;
use std::str;

pub struct Client {
    pub nick: String,
    pub first_seen: DateTime<Utc>
}

pub struct Peer {
    name: BytesMut,
    lines: Lines,
    state: Arc<Mutex<Shared>>,
    db_pool: Pool<PostgresConnectionManager>,
    rx: Rx,
    addr: SocketAddr
}

impl Client {
    pub fn new(nick: String, first_seen: DateTime<Utc> ) -> Self {
        return Client {
            nick, first_seen
        }
    }
}

impl Peer {
    pub fn new(name: BytesMut, state: Arc<Mutex<Shared>>, db_pool: Pool<PostgresConnectionManager>, lines: Lines) -> Peer {
        let addr = lines.socket.peer_addr().unwrap();
        let (tx, rx) = mpsc::unbounded();

        state.lock().unwrap().peers.insert(addr, tx);

        return Peer {
            name, lines, state, db_pool, rx, addr
        }
    }
}

impl Future for Peer {
    type Item = ();
    type Error = io::Error;

    fn poll(&mut self) -> Poll<(), io::Error> {
        const LINES_PER_TICK: usize = 10;

        for i in 0..LINES_PER_TICK {
            match self.rx.poll().unwrap() {
                Async::Ready(Some(v)) => {
                    self.lines.buffer(&v);

                    if i+1 == LINES_PER_TICK {
                        task::current().notify();
                    }
                }
                _ => break,
            }
        }

        let _ = self.lines.poll_flush()?;

        while let Async::Ready(line) = self.lines.poll()? {
            let line_temp= &self.name.clone();

            let nick_temp = match str::from_utf8(&line_temp) {
                Ok(v) => v,
                Err(e) => panic!("Invalid UTF-8 Sequence: {}", e)
            };

            let nick = String::from(nick_temp);

            if line == None {
                println!("{} disconnected", nick);
            }

            if let Some(message) = line {
                let now = Utc::now();

                let mut temp = self.name.clone();

                let message_temp = match str::from_utf8(&message) {
                    Ok(v) => v,
                    Err(e) => panic!("Invalid UTF-8 Sequence: {}", e)
                };

                let message_string = String::from(message_temp);

                let conn = self.db_pool.get().unwrap();
                let res = conn.execute("INSERT INTO messages (posted, posted_by, message) VALUES ($1, $2, $3)", &[&now, &nick, &message_string]);

                println!("{}: {}", nick, message_string);

                if res.is_ok() == false {
                    panic!("Unable to insert message!");
                }

                temp.extend_from_slice(b": ");
                temp.extend_from_slice(&message);
                temp.extend_from_slice(b"\r\n");

                let line = temp.freeze();

                for (addr, tx) in &self.state.lock().unwrap().peers {
                    if *addr != self.addr {
                        tx.unbounded_send(line.clone()).unwrap();
                    }
                }
            } else {
                return Ok(Async::Ready(()))
            }
        }

        return Ok(Async::NotReady)
    }
}

impl Drop for Peer {
    fn drop(&mut self) {
        self.state.lock().unwrap().peers.remove(&self.addr);
    }
}