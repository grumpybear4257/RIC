extern crate preferences;
extern crate r2d2_postgres;
extern crate chrono;
extern crate r2d2;
extern crate tokio;

#[macro_use]
extern crate futures;
extern crate bytes;

use std::thread;
use std::str;
use std::sync::Arc;
use std::sync::Mutex;
use std::collections::HashMap;

use chrono::prelude::*;
use tokio::prelude::*;
use tokio::net::TcpStream;
use tokio::prelude::future::Either;
use preferences::{ AppInfo, PreferencesMap, Preferences };
use r2d2_postgres::PostgresConnectionManager;
use r2d2_postgres::TlsMode;
use r2d2::Pool;

mod client;
mod network;

use client::Client;
use network::Shared;
use network::Lines;
use tokio::net::TcpListener;
use client::Peer;

const APP_INFO: AppInfo = AppInfo {
    name: "RIC", // Rust Internet Chat
    author: "Nathan Ford",
};
const DB_PREFS_KEY: &str = "database_config";

fn main() {
    /****************
    * Set up database for use
    ****************/
    let db_config= load_db_settings();
    println!("DB settings successfully loaded.");
    let db_pool: Pool<PostgresConnectionManager> = init_db_pool(&db_config.clone());
    println!("successfully connected to database.");

    create_tables(&db_pool);

    // This is super stupid and hacky and I hate it, but it's the only way I could figure out without rewriting most of what I have already.
    // Create a dedicated db pool for handling client/messages
    let db_pool_temp = init_db_pool(&db_config.clone());

    /****************
    * Setup Tokio
    ****************/
    let state = Arc::new(Mutex::new(Shared::new()));

    let server_addr = "127.0.0.1:4257".parse().unwrap();
    let tcp_listener = TcpListener::bind(&server_addr).unwrap();

    let server = tcp_listener.incoming().for_each(move |socket| {
        process(socket, state.clone(), db_pool_temp.clone());
        Ok(())
    }).map_err(|err| {
        println!("Accept error: {:?}", err);
    });

    tokio::run(server);

    println!("server started");
}

fn load_db_settings() -> HashMap<String, String> {
    println!("Trying to load DB Settings...");

    let load = PreferencesMap::<String>::load(&APP_INFO, DB_PREFS_KEY);
    let load_result = load.is_ok();

    if load_result == false {
        println!("Failed to load DB settings!");

        // Settings (probably) aren't initialized, so let's do that.
        init_db_settings();
        // Now they should be initialized, so use our function to load and ensure that all required settings are present.
        return load_db_settings();
    } else {
        // Unwrap the result object into a usable hashmap.
        let config = load.unwrap();
        let mut database_config: PreferencesMap<String> = PreferencesMap::new(); // This might not be used
        let mut changed: bool = false;

        // Check to make sure that the settings we need are set.
        if config.contains_key("user") == false {
            database_config.insert("user".into(), "ric".into());
            changed = true;
        }
        if config.contains_key("pass") == false {
            database_config.insert("pass".into(), "CHANGEME".into());
            changed = true;
        }
        if config.contains_key("host") == false {
            database_config.insert("host".into(), "localhost".into());
            changed = true;
        }
        if config.contains_key("port") == false {
            database_config.insert("port".into(), "5432".into());
            changed = true;
        }
        if config.contains_key("name") == false {
            database_config.insert("name".into(), "ric".into());
            changed = true;
        }

        if changed {
            // Insert the current values so we don't just save what we don't have (since then that's all we'll have)
            for (key, value) in config {
                database_config.insert(key.into(), value.into());
            }

            // Save the values to the file. Now we should have all the values we need.
            let save_result = database_config.save(&APP_INFO, DB_PREFS_KEY);
            assert!(save_result.is_ok());

            return PreferencesMap::<String>::load(&APP_INFO, DB_PREFS_KEY).unwrap();
        } else {
            return config;
        }
    }
}

fn init_db_settings() {
    println!("Initializing DB settings...");
    let mut database_config: PreferencesMap<String> = PreferencesMap::new();

    database_config.insert("user".into(), "ric".into());
    database_config.insert("pass".into(), "CHANGEME".into());
    database_config.insert("host".into(), "localhost".into());
    database_config.insert("port".into(), "5432".into());
    database_config.insert("name".into(), "ric".into());

    let save_result = database_config.save(&APP_INFO, DB_PREFS_KEY);
    assert!(save_result.is_ok());

    let test_result = PreferencesMap::<String>::load(&APP_INFO, DB_PREFS_KEY);
    assert!(test_result.is_ok());

    println!("Database settings initialized. You will likely need to change some config option to connect to a database.");
}

fn init_db_pool(config: &HashMap<String, String>) -> Pool<PostgresConnectionManager> {
    // One may ask now, why keep the table creation on it's own pool too? To which I reply: it works, I ain't touching it
    println!("Attempting to connect to PostgreSQL database");
    let connection_string: String = build_db_connection_string(config);

    let manager: PostgresConnectionManager = PostgresConnectionManager::new(connection_string, TlsMode::None).unwrap();
    return r2d2::Pool::builder().max_size(10).build(manager).unwrap();
}

fn build_db_connection_string(config: &HashMap<String, String>) -> String {
    let mut connection_string: String = String::from("postgres://");
    let mut user: &str = "";
    let mut pass: &str = "";
    let mut host: &str = "";
    let mut port: &str = "";
    let mut name: &str = "";

    for (key, value) in config.iter() {
        if key == "user" {
            if str::is_empty(&value) {
                panic!("Invalid user: user can't be empty");
            } else {
                user = value;
            }
        } else if key == "pass" {
            pass = value;
        } else if key == "host" {
            if str::is_empty(&value) {
                panic!("Invalid host: host can't be empty");
            } else {
                host = value;
            }
        } else if key == "port" {
            port = value;
        } else if key == "name" {
            name = value;
        }
    }

    connection_string.push_str(user);

    if str::is_empty(&pass) == false {
        connection_string.push(':');
        connection_string.push_str(&pass);
    }

    connection_string.push('@');
    connection_string.push_str(&host);

    if str::is_empty(&port) == false {
        connection_string.push(':');
        connection_string.push_str(&port);
    }

    if str::is_empty(&name) == false {
        connection_string.push('/');
        connection_string.push_str(&name);
    }

    return connection_string;
}

fn create_tables(db_pool: &Pool<PostgresConnectionManager>) {
    println!("Setting up database for use: Tables");
    let pool = db_pool.clone();
    let conn = pool.get().unwrap();

    // This database structure used to look OK, until I had to rework half my code to make it work.
    let res1 = conn.execute("CREATE TABLE IF NOT EXISTS clients (nick VARCHAR UNIQUE NOT NULL, first_seen TIMESTAMP WITH TIME ZONE NOT NULL)", &[]);
    let res2 = conn.execute("CREATE TABLE IF NOT EXISTS messages (posted TIMESTAMP WITH TIME ZONE NOT NULL, posted_by VARCHAR NOT NULL, message TEXT NOT NULL)", &[]);

    if res1.is_ok() == false || res2.is_ok() == false {
        panic!("Unable to create tables!");
    }

    println!("Setting up database for use: Tables -- Done");
}

fn on_client_connect(client: Client, pool: &Pool<PostgresConnectionManager>) {
    println!("{} connected", &client.nick);

    let pool_clone = pool.clone();

    let thread = thread::spawn(move || {
        let conn = pool_clone.get().unwrap();
        let res = conn.execute("INSERT INTO clients (nick, first_seen) VALUES ($1, $2) ON CONFLICT (nick) DO NOTHING", &[&client.nick, &client.first_seen]);

        if res.is_ok() == false {
            panic!("Unable to insert client into database");
        }
    });

    thread.join().unwrap();
}

fn process(socket: TcpStream, state: Arc<Mutex<Shared>>, pool: Pool<PostgresConnectionManager>) {
    let lines = Lines::new(socket);

    let connection = lines.into_future()
        .map_err(|(e, _)| e)
        .and_then(move |(name, lines)| {
            let name = match name {
                Some(name) => name,
                None => {
                    return Either::A(future::ok(()));
                }
            };

            // https://stackoverflow.com/questions/19076719
            let nick = match str::from_utf8(&name) {
                Ok(v) => v,
                Err(e) => panic!("Invalid UTF-8 Sequence: {}", e)
            };

            // It's the return of ya boi, hacky solution.
            let pool_temp = pool.clone();

            let client: Client = Client::new(String::from(nick), Utc::now());
            let peer: Peer = Peer::new(name.clone(), state, pool_temp, lines);

            on_client_connect(client, &pool);

            Either::B(peer)
        })
        .map_err(|e| {
            println!("Connection error: {:?}", e);
        });

    tokio::spawn(connection);
}